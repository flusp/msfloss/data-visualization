# MsFLOSS Data Visualization

This project encapsulates a **Ruby on Rails API** capable of fetching and
processing data from the available Bots, and a **JavaScript** frontend to
generate graphics and visualizations.

Currently the frontend is available at https://github.com/lucianokelvin/graficosPOO, and is to be integrated in this repository. The file api/sample.html provides example charts containing data fetched from the Ruby API.

# REQUIREMENTS
Ruby >= 2.6.0  
Rails >= 5.0.7.2  
Bundler >= 2.0.1  
Puma >= 3.0  
rack-cors  
byebug  
rspec  
rspec-rails  
listen >= 3.0.5  
spring  
spring-watcher-listen >= 2.0.0  
tzinfo-data  

# SETUP

Open the Ruby on Rails API folder
```console
$ cd RailsAPI
```
Recommended running
```console
$ gem update --system
```
Due to a possible Bundler::LockfileError
```console
$ bundle install
```
```console
$ rails server
```

Alternatively, if you have `docker` and `docker-compose` installed,
you can simply run
```console
$ docker-compose up
```
from the RailsAPI folder.

The server will be available at `http://localhost:1313/`.

# TESTING
```console
$ bundle exec rspec
```
