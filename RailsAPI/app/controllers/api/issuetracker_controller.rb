require 'net/http'
require_relative 'api_tool/parJson.rb'

class Api::IssuetrackerController < ApplicationController

  # GET ranking/repositories
  # Returns repositories grouped and ordered by number of issues and authors
  def ranking_repositories
      # Mocked data
      data = [1,2,3]
      render json: data, status: :ok
  end

  # GET /issuetracker/issues_closed
  # Returns issues closed by commits
  def issues_closed

    url = URI("http://msfloss.interscity.org/issues/ranking/users-close")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url)
    request["Access-Control-Allow-Origin"] = '*'
    request["Access-Control-Allow-Credentials"] = 'true'
    response = http.request(request)
    
    dataExtract = DataJsonExtract.new
    data        = dataExtract.makeTableDataIssueTracker(response, params[:order], params[:limit].to_i)
    render json: data, status: :ok

  end


end
