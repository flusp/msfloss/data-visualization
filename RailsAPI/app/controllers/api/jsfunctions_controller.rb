#require 'net/http'

class Api::JsfunctionsController < ApplicationController

    # GET jsfunctions/functions_list
    # Returns an JSON containing the chart plotting functions available in the
    # JavaScript
    def functions_list
      data = Jsfunctions.new
      data.get_functions
      render json: data, status: :ok
    end
  
  end