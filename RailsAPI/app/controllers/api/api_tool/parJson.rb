require 'json'
require 'net/http'
require 'uri'
require 'time'
require 'chronic'
require_relative 'timeTool.rb'

class DataJsonExtract


#Issue@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  def makeTableDataIssueTracker(dataJson, order, limit)
    
    parsed = JSON.parse(dataJson.body)
    login  = Hash.new

    for i in parsed do
      if limit > 0 then
        login[i["login"]] = i["issuesClosed"]
      end
      limit -= 1
    end

    if order == "decrescent" then
      loginSort    = login.sort { |l, r| l[1]<=>r[1] }.reverse
    else
      loginSort    = login.sort { |l, r| l[1]<=>r[1] }
    end

    return loginSort.to_h.to_json

  end


#Email@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  def makeTableDataEmailMessageSize(dataJson, order, limit)

    parsed       = JSON.parse(dataJson.body)
    email        = Hash.new
    emailSort    = Hash.new
    emailSortTen = Hash.new

    for i in parsed do
      
      message              = i['message']
      email[i["email_id"]] = message.size

    end
    
    if order == "decrescent" then
      emailSort    = email.sort { |l, r| l[1]<=>r[1] }.reverse
    else
      emailSort    = email.sort { |l, r| l[1]<=>r[1] }
    end
    emailSortTen = emailSort.first(limit).to_h.map {|k, v| ["EmailID: "+k.to_s, v]}.to_h

    return emailSortTen.to_json

  end

  def makeTableDataEmailNumParticipants(dataJson, order, limit)

    parsed = JSON.parse(dataJson.body)
    message_start_subject      = []
    conversationNumMembers     = Hash.new
    conversationNumMembersSort = Hash.new
    conversationNumMembersTen  = Hash.new
    numMembers                 = Hash.new

    for i in parsed do

      if i["in_reply_to"] == "" then

        message_start_subject.append(i['subject'])

      end

    end

    for j in message_start_subject do

      for i in parsed do

        str = i['subject']

        if str.include? j then

          if !numMembers.has_key?(i['sender']) then

            numMembers[i['sender']] = 1
    
          end

        end

      end

      num = numMembers.length
      conversationNumMembers[j] =  num
      numMembers   = Hash.new

    end

    if order == "decrescent" then
      conversationNumMembersSort    = conversationNumMembers.sort { |l, r| l[1]<=>r[1] }.reverse
    else
      conversationNumMembersSort    = conversationNumMembers.sort { |l, r| l[1]<=>r[1] }
    end
    conversationNumMembersTen     = conversationNumMembersSort.first(limit).to_h

    return conversationNumMembersTen.to_json

  end

  def makeTableDataEmailLifetimeConversation(dataJson, order, limit)

    parsed                 = JSON.parse(dataJson.body)
    message_start_subject  = []
    message_Conversation   = []
    conversationTime       = Hash.new
    conversationPeriod     = Hash.new
    conversationPeriodSort = Hash.new
    conversationPeriodTen  = Hash.new
    farDate                = 0
    timeOper               = TimerOperator.new

    for i in parsed do

      if i["in_reply_to"] == "" then

        message_start_subject.append(i['subject'])
        conversationTime[i['subject']] = i['date']

      end

    end

    for j in message_start_subject do
  
      for i in parsed do

        str = i['subject']

        if str.include? j then

          message_Conversation.append(i['date'])

        end

      end

      farDate = conversationTime[j]

      for w in message_Conversation do
  
        if Chronic.parse(farDate) < Chronic.parse(w) then
          farDate = w
        end

      end
  
      message_Conversation = []
      inicConv = Chronic.parse(conversationTime[j])
      endConv  = Chronic.parse(farDate)
      period   = endConv - inicConv
      conversationPeriod[j] = timeOper.timePeriod(period).round

    end
    if order == "decrescent" then
      conversationPeriodSort    = conversationPeriod.sort { |l, r| l[1]<=>r[1] }.reverse
    else
      conversationPeriodSort    = conversationPeriod.sort { |l, r| l[1]<=>r[1] }
    end
    conversationPeriodTen       = conversationPeriodSort.first(limit).to_h

    return conversationPeriodTen.to_json

  end

#Git@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  def makeTableDataGitContribution(dataJson, order, limit)
    parsed = JSON.parse(dataJson.body)
    data = Hash.new
    dataSorted = Hash.new

    for item in parsed do
      days = (DateTime.iso8601(item["last_contribution"]) - DateTime.iso8601(item["first_contribution"])).to_i
      data[item["name"]] = days
    end

    if order == "decrescent" then
      dataSorted = data.sort_by{|k, v| -v}
    else
      dataSorted = data.sort_by{|k, v| v}
    end

    return dataSorted.first(limit).to_h.to_json

  end

  def makeTableDataGitCommit(dataJson, order, limit)

    parsed           = JSON.parse(dataJson.body)
    userCommitNumber = Hash.new
    commitSort       = Hash.new
    commitTop     = Hash.new
    commitArr        = Array.new
    cont             = 1

    for item in parsed do
   
       if !userCommitNumber.has_key?(item['name']) then

           userCommitNumber[item['name']] = item['commits'].length

       else 

           cont = cont + 1
	         userCommitNumber[item['name']+ ' ' + (cont).to_s] = item['commits'].length

       end       
       
    end

    if order == "decrescent" then
      commitSort   = userCommitNumber.sort { |l, r| l[1]<=>r[1] }.reverse
    else
      commitSort   = userCommitNumber.sort { |l, r| l[1]<=>r[1] }
    end

    commitTop = commitSort.first(limit).to_h

    return commitTop.to_json

  end

  def makeTableDataGitContributosNew(dataJson)
    
    parsed                     = JSON.parse(dataJson.body)
    contribuInicData           = Hash.new
    contribuSort               = Hash.new
    contributTopTen            = Hash.new
    sizeList                   = 0
    contribuSortAfterFirstDate = Hash.new
    cont                       = 1
    contName                   = 1

     for item in parsed do

        if !contribuInicData.has_key?(item['name']) then

           contribuInicData[item['name']] = DateTime.iso8601(item['first_contribution'])

       else

           contName = contName + 1
           contribuInicData[item['name'] + ' ' + (contName).to_s] = DateTime.iso8601(item['first_contribution'])

       end  

    end

    contribuSort = contribuInicData.sort { |l, r| l[1]<=>r[1] }
    sizeList = contribuSort.length
    arrayKey = contribuSort.collect {|key,value| key }
    
    for i in arrayKey do

       contribuSortAfterFirstDate[i] = sizeList - cont
       cont = cont + 1	

    end    

    contributTopTen = contribuSortAfterFirstDate.first(10).to_h

    return contributTopTen.to_json

  end


#IRC#################################################################################################

  def makeTableDataIRCmessages(dataJson)
    parsed = JSON.parse(dataJson.body)
    data = Hash.new
    # get number of messages by hour
    for item in parsed do
      hour = DateTime.parse(item["time"]).hour
      if data[hour.to_s+'h'] then
        data[hour.to_s+'h'] += 1
      else
        data[hour.to_s+'h'] = 1
      end
    end
    # complete hash table with the other hours
    for i in 0...24 do
      data[i.to_s+'h'] = 0 unless data[i.to_s+'h']
    end
    # return sorted hash to json
    return data.sort_by{|k, v| k[0...-1].to_i}.to_h.to_json
 end

#NLP@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  def parseEmailMessage(message)
    return message.gsub(/\r?\n|\r|\t|\u003e/, '').partition("diff --git")[0]
  end

  def makeTableDataNLPEmailMessages(dataJson, firstTime)

    subjects = JSON.parse(dataJson.body).first(10)
    subjects_analysis = Hash.new
    
    conversations = []
    if !firstTime then
      url = URI("http://msfloss-nlp.herokuapp.com/conversation/")
      http = Net::HTTP.new(url.host, url.port)
      request = Net::HTTP::Get.new(url)
      request["Access-Control-Allow-Origin"] = '*'
      request["Access-Control-Allow-Credentials"] = 'true'
      response = http.request(request)
      parsed = JSON.parse(response.body)["conversations"]
      for item in parsed do
        conversations.append(item["id"]) if subjects.include?(item["context"]["subject"])
      end
    else
      for subject in subjects do
        subject = subject[4..-1] if subject[0...3] == "Re:" or subject[0...3] == "RE:"
    
        # Adding a new conversation (subject) using the NLP service
        url = URI("http://msfloss-nlp.herokuapp.com/conversation/")
        http = Net::HTTP.new(url.host, url.port)
        request = Net::HTTP::Post.new(url)
        request["content-type"] = 'application/json'
        request["Access-Control-Allow-Origin"] = '*'
        request["Access-Control-Allow-Credentials"] = 'true'
        request.body = {"source" => "MAIL", "context" => { "name" => "Linux IIO mail list", "subject" => "#{subject}"}}.to_json
        response = http.request(request)
        conversation_id = JSON.parse(response.body)["uuid"]
        conversations.append(conversation_id)

        # Get all emails with the same subject
        url = URI("http://limitless-taiga-45801.herokuapp.com/api/v1/emails/query?subject_contains=#{subject}")
        http = Net::HTTP.new(url.host, url.port)
        request = Net::HTTP::Get.new(url)
        request["Access-Control-Allow-Origin"] = '*'
        request["Access-Control-Allow-Credentials"] = 'true'
        response = http.request(request)
        emails = JSON.parse(response.body)

        # Adding an event (message) using the NLP service
        for email in emails do
          message = parseEmailMessage(email["message"])

          url = URI("http://msfloss-nlp.herokuapp.com/event/#{conversation_id}")
          http = Net::HTTP.new(url.host, url.port)
          request = Net::HTTP::Post.new(url)
          request["content-type"] = 'application/json'
          request["Access-Control-Allow-Origin"] = '*'
          request["Access-Control-Allow-Credentials"] = 'true'
          request.body = {"user" => "#{email['sender']}","timestamp" => "#{email['date']}", "type" => "MSG", "content" => "#{message}"}.to_json
          response = http.request(request)
        end
      end
    end
  
    # Analyze email messages using the NLP service
    for index in 0...10 do
      scores = Hash.new
      scores["neg"] = 0
      scores["neu"] = 0
      scores["pos"] = 0
      scores["compound"] = 0
      size = 0
      conversation_id = conversations[index]

      url = URI("http://msfloss-nlp.herokuapp.com/data/sentiment/#{conversation_id}")
      http = Net::HTTP.new(url.host, url.port)
      request = Net::HTTP::Get.new(url)
      request["Access-Control-Allow-Origin"] = '*'
      request["Access-Control-Allow-Credentials"] = 'true'
      response = http.request(request)
      parsed = JSON.parse(response.body)["events"]

      for msg in parsed do
        # scores = messages scores average
        if msg["conversation"] == conversation_id then
          scores["neg"] += msg["scores"]["neg"]
          scores["neu"] += msg["scores"]["neu"]
          scores["pos"] += msg["scores"]["pos"]
          scores["compound"] += msg["scores"]["compound"]
          size += 1
        end
      end

      scores = scores.map { |k,v| [k, v.to_f/size] }.to_h

      # normalizing (0 to 100) - the bigger, the most negative sentiment
      xmin = -1.0
      xmax = 1.0
      ymin = 0.0
      ymax = 100.0
      xrange = xmax-xmin
      yrange = ymax-ymin
      score = ymin + (scores["compound"]-xmin) * (yrange / xrange)

      subjects_analysis[subjects[index]] = (100 - score).to_f.round(1)
    end

    return subjects_analysis.sort_by{ |k, v| -v }.to_h.to_json

  end

end
