#################
#Alexandre/Bruna#
#05/11/2019     #
#################

require 'rubygems' # if not using Bundler
require 'simple_stats'

class BasicStatisticTool

	##########################################
	#Receives an array of numbers and returns# 
	# a numeric array with the values of     # 
	# sum, mean, median, modes, and          #
	# standard_deviation                     #
	# ########################################

	def statistic (data)
            
		data_stati = Array.new

		data_stati.push data.sum
		data_stati.push data.mean
		data_stati.push data.median
		data_stati.push data.modes
                data_stati.push standard_deviation(data, data.mean)
		data_stati.push data.sort
                
		return data_stati

	end

        ##########################################
	#Receives a number array and the mean and# 
	#returns the standard deviation.         # 
	##########################################
	
	def standard_deviation (data, mean)

		sum = 0
                
		for i in data do
      
                    sum = sum + (i - mean)**2     
   
               end
               
               variance = sum/data.length
	       
	       return (variance**0.5)

	end

end
