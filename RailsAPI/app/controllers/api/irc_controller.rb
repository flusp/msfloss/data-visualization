require 'net/http'
require_relative 'api_tool/parJson.rb'

class Api::IrcController < ApplicationController

  # GET irc/users_participation
  # Returns the number of active users per hour along the day
  def users_participation
    # Mocked data
    data = {'00:00' => 23 , '01:00'  => 45 , '02:00'  => 10 , '03:00'  => 23 , '04:00' => 45 , '05:00' => 50 , '06:00' => 19 , '07:00' => 30}
    render json: data, status: :ok
  end

  # GET irc/number_messages
  # Returns the number of sent messages per hour along the day
  def number_messages

    if params[:repo] != "" then
      url = URI("http://msfloss.interscity.org/irc/messages?channel=%23#{params[:repo]}")
    else 
      url = URI("http://msfloss.interscity.org/irc/messages")
    end
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url)
    request["Access-Control-Allow-Origin"] = '*'
    request["Access-Control-Allow-Credentials"] = 'true'
    response = http.request(request)
    
    dataExtract = DataJsonExtract.new
    data        = dataExtract.makeTableDataIRCmessages(response)
    render json: data, status: :ok

  end

end
