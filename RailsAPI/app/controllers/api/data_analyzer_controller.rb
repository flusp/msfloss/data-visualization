###################
#Alexandre/Bruna  #
#05/10/2019       #
###################

require 'net/http'
require 'rubygems'
require_relative 'api_tool/statistic.rb'

class Api::DataAnalyzerController < ApplicationController

  # TEMPLATE:
  #response = Net::HTTP.get_response('data_repository_url', '/endpoint', <optional_port_number>)
  #render json: response.body, status: :ok

  ##################################################################
  #Receive a data sequence and returns a json with basic statistics#
  ##################################################################
  def get_data
        
    static_tool = BasicStatisticTool.new
    data_stati = Array.new

    #Mocked data
    data = [30,22,40,40,40,60]
    data_stati = static_tool.statistic(data) 
     
    
    render json: data_stati, status: :ok

  end

end

