require 'net/http'
require_relative 'api_tool/parJson.rb'

class Api::GitController < ApplicationController

  # GET git/commits
  # Returns the number of commits by user
  def commits
    
    rid = (params[:repo] != "" ? params[:repo] : 1)
    url = URI("http://msfloss.interscity.org/commit/api/v1/contributors?rid=#{rid}")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url)
    request["Access-Control-Allow-Origin"] = '*'
    request["Access-Control-Allow-Credentials"] = 'true'
    response = http.request(request)

    dataExtract = DataJsonExtract.new
    data        = dataExtract.makeTableDataGitCommit(response, params[:order], params[:limit].to_i)
    render json: data, status: :ok

  end

  # GET git/commits_reviews
  # Returns the number of commits and reviews by user, respectively
  def commits_reviews
    # Mocked data
    users = ["Maria", "Joao", "Joana"]
    commits = [10, 20, 10]
    reviews = [5, 12, 21]
    data = [users, commits, reviews]
    render json: data, status: :ok
  end

  # GET git/contributors_new
  # Returns the quantity of new contributors since a specific date
  def contributors_new
    
    rid = (params[:repo] != "" ? params[:repo] : 1)
    url = URI("http://msfloss.interscity.org/commit/api/v1/contributors?rid=#{rid}")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url)
    request["Access-Control-Allow-Origin"] = '*'
    request["Access-Control-Allow-Credentials"] = 'true'
    response = http.request(request)

    dataExtract = DataJsonExtract.new
    data        = dataExtract.makeTableDataGitContributosNew(response)
    render json: data, status: :ok

  end

  # GET git/contribution_period
  # Returns the quantity of days of users contributions
  def contribution_period

    rid = (params[:repo] != "" ? params[:repo] : 1)
    url = URI("http://msfloss.interscity.org/commit/api/v1/contributors?rid=#{rid}")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url)
    request["Access-Control-Allow-Origin"] = '*'
    request["Access-Control-Allow-Credentials"] = 'true'
    response = http.request(request)
    
    dataExtract = DataJsonExtract.new
    data        = dataExtract.makeTableDataGitContribution(response, params[:order], params[:limit].to_i)
    render json: data, status: :ok

  end

end
