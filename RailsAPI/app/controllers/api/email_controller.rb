require 'net/http'
require_relative 'api_tool/parJson.rb'


class Api::EmailController < ApplicationController

  # GET email/message_size
  # Returns an array of integers containing the message sizes
  def message_size

    url = URI("http://limitless-taiga-45801.herokuapp.com/api/v1/emails")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url)
    request["Access-Control-Allow-Origin"] = '*'
    request["Access-Control-Allow-Credentials"] = 'true'
    response = http.request(request)

    dataExtract = DataJsonExtract.new
    data        = dataExtract.makeTableDataEmailMessageSize(response, params[:order], params[:limit].to_i)
    
    render json: data, status: :ok

  end

  # GET email/num_participants
  # Returns one array of integers formed by the number of participants
  def num_participants
    
    url = URI("http://limitless-taiga-45801.herokuapp.com/api/v1/emails")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url)
    request["Access-Control-Allow-Origin"] = '*'
    request["Access-Control-Allow-Credentials"] = 'true'
    response = http.request(request)

    dataExtract = DataJsonExtract.new
    data        = dataExtract.makeTableDataEmailNumParticipants(response, params[:order], params[:limit].to_i)
    
    render json: data, status: :ok

  end

  # GET email/lifetime_conversation
  # Returns the lifetime (in days) for each conversation theme
  def lifetime_conversation

    url = URI("http://limitless-taiga-45801.herokuapp.com/api/v1/emails")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url)
    request["Access-Control-Allow-Origin"] = '*'
    request["Access-Control-Allow-Credentials"] = 'true'
    response = http.request(request)

    dataExtract = DataJsonExtract.new
    data        = dataExtract.makeTableDataEmailLifetimeConversation(response, params[:order], params[:limit].to_i)
    
    render json: data, status: :ok
  end

  # GET email/developer_behavior
  # Returns one hash formed by pairs ("DeveloperName", "FriendlyRate*100")
  def developer_behavior
    # Mocked data
    data = {"Maria" => 90, "Joao" => 75, "Joana" => 50, "Marcelo" => 30, "Beatriz" => 10}
    render json: data, status: :ok
  end

  # GET email/number_msg_topic
  # Returns one hash formed by pairs ("TopicName", "NumberOfMessages")
  def number_msg_topic
    # Mocked data
    data = {"T1" => 9, "T2" => 7, "T3" => 15, "T4" => 8, "T5" => 13, "T6" => 15}
    render json: data, status: :ok
  end

  # GET email/importance_discussions
  # Returns one hash containing the number of messages by theme and date
  def importance_discussions
    # Mocked data
    dataTheme1 = {"12/01/2018" => 9, "13/01/2018" => 20, "14/01/2018" => 12, "15/01/2018" => 5, "16/01/2018" => 23}
    dataTheme2 = {"12/01/2018" => 12, "13/01/2018" => 30, "14/01/2018" => 5, "15/01/2018" => 22, "16/01/2018" => 30}
    data = {"theme 1" => dataTheme1, "theme 2" => dataTheme2}
    render json: data, status: :ok
  end

  # GET email/messages_x_counts_users
  # Returns one hash containing themes and its number of users and messages, respectively
  def messages_x_counts_users
    # Mocked data    
    data = {"theme 1" => [10, 20], "theme 2" => [5, 12], "Theme3" => [22, 30]}
    render json: data, status: :ok
  end

  # GET email/subjects_NLP_analysis
  # Returns one hash containig the scores of 10 different subjects
  def subjects_NLP_analysis

    url = URI("http://limitless-taiga-45801.herokuapp.com/api/v1/subjects")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url)
    request["Access-Control-Allow-Origin"] = '*'
    request["Access-Control-Allow-Credentials"] = 'true'
    response = http.request(request)

    dataExtract = DataJsonExtract.new
    data        = dataExtract.makeTableDataNLPEmailMessages(response, false)
    
    render json: data, status: :ok
  end

end
