class Jsfunctions

  def initialize
    @functions_list = []
  end

  def get_functions
    #@functions_list.push('teste')
    curr_path = Dir.pwd
    subdir = '/Javascript/api/'
    filename = 'api.js'
    regex = /^async function (.*)\(/

    path = curr_path + subdir + filename
    File.open(path).each do |line|
      f_name = line.match(regex)
      unless f_name.nil?
        @functions_list.push(f_name[1])
      end
    end

    @functions_list
  end

  def show
    @functions_list
  end

end 
