require 'rails_helper'
require 'spec_helper'
require 'json'

RSpec.describe 'email API', type: :request do

  path = '/RailsAPI/spec/support/email/'
  set_order = 'decrescent'
  set_limit = '10'
  set_firstTime = 'false'

  describe 'GET /email/message_size' do
    before {get '/email/message_size', :params => {'order':set_order, 'limit':set_limit}}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
      result = JSON.parse(response.body)
      expected_load = File.open((File.dirname(Dir.pwd) + path + 'message_size.json'), 'rb').read
      expected_result = JSON.parse(expected_load)
      expect(result).to eq(expected_result)
    end
  end

  describe 'GET /email/num_participants' do
    before {get '/email/num_participants', :params => {'order':set_order, 'limit':set_limit}}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
      result = JSON.parse(response.body)
      expected_load = File.open((File.dirname(Dir.pwd) + path + 'num_participants.json'), 'rb').read
      expected_result = JSON.parse(expected_load)
      expect(result).to eq(expected_result)
    end
  end

  describe 'GET /email/lifetime_conversation' do
    before {get '/email/lifetime_conversation', :params => {'order':set_order, 'limit':set_limit}}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
      result = JSON.parse(response.body)
      expected_load = File.open((File.dirname(Dir.pwd) + path + 'lifetime_conversation.json'), 'rb').read
      expected_result = JSON.parse(expected_load)
      expect(result).to eq(expected_result)
    end
  end

  describe 'GET /email/developer_behavior #TODO' do
    before {get '/email/developer_behavior'}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
      # TODO
    end
  end

  describe 'GET /email/number_msg_topic #TODO' do
    before {get '/email/number_msg_topic'}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
      # TODO
    end
  end

  describe 'GET /email/importance_discussions #TODO' do
    before {get '/email/importance_discussions'}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
      # TODO
    end
  end

  describe 'GET /email/messages_x_counts_users #TODO' do
    before {get '/email/messages_x_counts_users'}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
      # TODO
    end
  end

  describe 'GET /email/subjects_NLP_analysis' do
    before {get '/email/subjects_NLP_analysis', :params => {'firstTime':set_firstTime}}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
      result = JSON.parse(response.body)
      expected_load = File.open((File.dirname(Dir.pwd) + path + 'subjects_NLP_analysis.json'), 'rb').read
      expected_result = JSON.parse(expected_load)
      expect(result).to eq(expected_result)
    end
  end

end
