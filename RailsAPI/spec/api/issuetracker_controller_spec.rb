require 'rails_helper'
require 'spec_helper'
require 'json'

RSpec.describe 'issuetracker API', type: :request do

  path = '/RailsAPI/spec/support/issuetracker/'
  set_order = 'decrescent'
  set_limit = '10'
  
  describe 'GET /issuetracker/ranking_repositories #TODO' do
    before {get '/issuetracker/ranking_repositories'}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
    
    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
        # TODO
    end
  end

  describe 'GET /issuetracker/issues_closed' do
    before {get '/issuetracker/issues_closed', :params => {'order':set_order, 'limit':set_limit}}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
        result = JSON.parse(response.body)
        expected_load = File.open((File.dirname(Dir.pwd) + path + 'issues_closed.json'), 'rb').read
        expected_result = JSON.parse(expected_load)
        expect(result).to eq(expected_result)
    end
  end

end
