require 'rails_helper'
require 'spec_helper'
require 'json'

RSpec.describe 'git API', type: :request do

  path = '/RailsAPI/spec/support/git/'
  set_order = 'decrescent'
  set_limit = '10'

  describe 'GET /git/commits' do
    before {get '/git/commits', :params => {'order':set_order, 'limit':set_limit}}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
      result = JSON.parse(response.body)
      expected_load = File.open((File.dirname(Dir.pwd) + path + 'commits.json'), 'rb').read
      expected_result = JSON.parse(expected_load)
      expect(result).to eq(expected_result)
    end
  end

  describe 'GET /git/commits_reviews #TODO' do
    before {get '/git/commits_reviews'}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
      # TODO
    end
  end

  describe 'GET /git/contributors_new' do
    before {get '/git/contributors_new'}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
      result = JSON.parse(response.body)
      expected_load = File.open((File.dirname(Dir.pwd) + path + 'contributors_new.json'), 'rb').read
      expected_result = JSON.parse(expected_load)
      expect(result).to eq(expected_result)
    end
  end

  describe 'GET /git/contribution_period' do
    before {get '/git/contribution_period', :params => {'order':set_order, 'limit':set_limit}}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
      result = JSON.parse(response.body)
      expected_load = File.open((File.dirname(Dir.pwd) + path + 'contribution_period.json'), 'rb').read
      expected_result = JSON.parse(expected_load)
      expect(result).to eq(expected_result)
    end
  end

end
