require 'rails_helper'
require 'spec_helper'
require 'json'

RSpec.describe 'irc API', type: :request do

  path = '/RailsAPI/spec/support/irc/'

  describe 'GET /irc/users_participation #TODO' do
    before {get '/irc/users_participation'}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
    
    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
        # TODO
    end
  end

  describe 'GET /irc/number_messages' do
    before {get '/irc/number_messages'}

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a JSON' do
      expect(response.content_type).to eq(Mime[:json])
    end

    it 'returns the expected result' do
        result = JSON.parse(response.body)
        expected_load = File.open((File.dirname(Dir.pwd) + path + 'number_messages.json'), 'rb').read
        expected_result = JSON.parse(expected_load)
        expect(result).to eq(expected_result)
    end
  end

end