require 'sinatra/base'

class FakeServer < Sinatra::Base

  path_email = '/email/'
  path_git = '/git/'
  path_irc = '/irc/'
  path_issuetracker = '/issuetracker/'

  # email
  # message_size num_participants lifetime_conversation developer_behavior
  # number_msg_topic importance_discussion messages_x_count_users
  get '/api/v1/emails' do
    json_response 200, path_email + 'emails.json'
  end
  # subjects_NLP_analysis
  get '/api/v1/subjects' do
    json_response 200, path_email + 'subjects.json'
  end

  # git
  # commits commits_review contributors_new contribution_period
  get '/commit/api/v1/contributors' do
    json_response 200, path_git + 'contributors.json'
  end

  # irc
  # users_participation number_messages
  get '/irc/messages' do
    json_response 200, path_irc + 'messages.json'
  end

  # issuetracker
  # ranking_repositories issues_closed
  get '/issues/ranking/users-close' do
    json_response 200, path_issuetracker + 'users-close.json'
  end

  private

  def json_response(response_code, file_path)
    content_type :json
    status response_code
    File.open(File.dirname(__FILE__) + file_path, 'rb').read
  end

end