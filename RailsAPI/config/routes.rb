Rails.application.routes.draw do
  namespace :api, :path => "" do
    
    resources :email do
      get :message_size, on: :collection
      get :num_participants, on: :collection
      get :lifetime_conversation, on: :collection
      get :developer_behavior, on: :collection
      get :number_msg_topic, on: :collection
      get :importance_discussions, on: :collection
      get :messages_x_counts_users, on: :collection
      get :subjects_NLP_analysis, on: :collection
    end
    
    resources :git do
      get :commits, on: :collection
      get :commits_reviews, on: :collection
      get :contributors_new, on: :collection
      get :contribution_period, on: :collection
    end

    resources :irc do
      get :users_participation, on: :collection
      get :number_messages, on: :collection
    end

    resources :issuetracker do
      get :ranking_repositories, on: :collection
      get :issues_closed, on: :collection
    end

    resources :data_analyzer do
      get :get_data, on: :collection
    end
    
    resources :jsfunctions do
      get :functions_list, on: :collection
    end

  end
end
